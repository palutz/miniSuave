module JwtToken

open Encodings
open System
open System.Security.Claims
open System.IdentityModel.Tokens
open System.Security.Cryptography

type TokenCreateRequest = {         
  Issuer : string        
  UserName : string
  Password : string        
  TokenTimeSpan : TimeSpan
}

type IdentityStore = {
  getClaims : string -> Async<Claim seq>
  isValidCredentials : string -> string -> Async<bool>
  getSecurityKey : Base64String -> SecurityKey
  getSigningCredentials : SecurityKey -> SigningCredentials
}

type Token = {
  AccessToken : string        
  ExpiresIn : float        
} 

type Audience = {
  ClientId : string
  Secret : Base64String
  Name : string
}

// string -> Audience
let createAudience audienceName =
  let clientId = Guid.NewGuid().ToString("N")
  let data = Array.zeroCreate 32
  RNGCryptoServiceProvider.Create().GetBytes(data)
  let secret = data |> Base64String.create
  {ClientId = clientId; Secret = secret; Name = audienceName}

// r = request to create token
let createToken r idStore audience = 
  async {
    let! isValidCredentials = idStore.isValidCredentials r.UserName r.Password
    if isValidCredentials then                            
      let signCred = audience.Secret |> (idStore.getSecurityKey >> idStore.getSigningCredentials) 
      let issuedOn = Nullable DateTime.UtcNow
      let expiresBy = Nullable (DateTime.UtcNow.Add(r.TokenTimeSpan))       
      let! claims =  idStore.getClaims r.UserName 
      let jwtToken = new JwtSecurityToken(r.Issuer, audience.ClientId, claims, issuedOn, expiresBy, signCred)
      let h = new JwtSecurityTokenHandler()
      let accessToken = h.WriteToken(jwtToken)                
      return Some {AccessToken = accessToken; ExpiresIn = r.TokenTimeSpan.TotalSeconds}
    else return None 
  }


type TokenValidationRequest = {
  Issuer : string
  SecurityKey : SecurityKey
  ClientId : string
  AccessToken : string
}

// Validate the token request
let validate t = 
  let tokenValidationParameters =
    let vPar = new TokenValidationParameters()
    vPar.ValidAudience <- t.ClientId
    vPar.ValidIssuer <- t.Issuer
    vPar.ValidateLifetime <- true
    vPar.ValidateIssuerSigningKey <- true
    vPar.IssuerSigningKey <-  t.SecurityKey
    vPar    
  
  try 
    let h = new JwtSecurityTokenHandler() 
    let principal = h.ValidateToken(t.AccessToken, tokenValidationParameters, ref null)
    principal.Claims |> Choice1Of2
  with
  | ex -> ex.Message |> Choice2Of2
