module SuaveJson 

open Newtonsoft.Json
open Newtonsoft.Json.Serialization

open Suave.Http
open Suave.Successful
open Suave.Operators
open Suave


let JSON v =     
  let jSett = new JsonSerializerSettings()
  jSett.ContractResolver <- new CamelCasePropertyNamesContractResolver()
  
  JsonConvert.SerializeObject(v, jSett)
  |> OK 
  >=> Writers.setMimeType "application/json; charset=utf-8"


let mapJsonPayload<'a> (req : HttpRequest) = 
  let fromJson j =
    try 
      let o = JsonConvert.DeserializeObject(j, typeof<'a>) :?> 'a    
      Some o
    with
    | _ -> None

  let getString r = System.Text.Encoding.UTF8.GetString(r)
  req.rawForm |> getString |> fromJson
