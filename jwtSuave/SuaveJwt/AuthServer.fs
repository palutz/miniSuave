module AuthServer

open Suave.Http
open JwtToken
open System
open SuaveJson
open Suave.RequestErrors
open Suave
open Suave.Filters
open Suave.Operators

type AudienceCreateRequest = {
  Name : string
}

type AudienceCreateResponse = {
  ClientId : string
  Base64Secret : string
  Name : string
}

type TokenCreateCredentials = {
  UserName : string
  Password : string
  ClientId : string
}

type Config = {
  AddAudienceUrlPath : string
  CreateTokenUrlPath : string
  SaveAudience : Audience -> Async<Audience>
  GetAudience : string -> Async<Audience option>
  Issuer : string
  TokenTimeSpan : TimeSpan
}

let audienceWebPart config idStore =

  let toAudienceCreateResponse (audience : Audience) = {
    Base64Secret = audience.Secret.ToString()
    ClientId = audience.ClientId        
    Name = audience.Name
  }

  let tryCreateAudience (ctx: HttpContext) =
    match ctx.request |> mapJsonPayload<AudienceCreateRequest> with
    | Some audienceCreateReq -> 
        async {
          let! a = audienceCreateReq.Name |> createAudience |> config.SaveAudience                     
          let audienceCreateResponse = a |> toAudienceCreateResponse
          return! JSON audienceCreateResponse ctx
        }
    | None -> BAD_REQUEST "Invalid Audience Create Request" ctx

  let tryCreateToken (ctx: HttpContext) =
    match ctx.request |> mapJsonPayload<TokenCreateCredentials>  with
    | Some tCreateCredentials -> 
        async {
          let! audience = config.GetAudience tCreateCredentials.ClientId
          match audience with
          | Some a ->
              let tCreateReq : TokenCreateRequest = {         
                Issuer = config.Issuer        
                UserName = tCreateCredentials.UserName
                Password = tCreateCredentials.Password        
                TokenTimeSpan = config.TokenTimeSpan
              }
              let! token = createToken tCreateReq idStore a
              match token with
              | Some t -> return! JSON t ctx
              | None -> return! BAD_REQUEST "Invalid Login Credentials" ctx
              
          | None -> return! BAD_REQUEST "Invalid Client Id" ctx
        }
    | None -> BAD_REQUEST "Invalid Token Create Request" ctx

  choose [
    path config.AddAudienceUrlPath >=> POST >=> tryCreateAudience
    path config.CreateTokenUrlPath >=> POST >=> tryCreateToken
  ]
