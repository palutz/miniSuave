namespace SuaveRestApi.Rest

open Newtonsoft.Json
open Newtonsoft.Json.Serialization
open Suave
open Suave.Operators
open Suave.Http
open Suave.Successful
open Suave.RequestErrors
open Suave.Filters

[<AutoOpen>]
module RestFul =
  
  type RestResource<'a> = {
    GetAll : unit -> 'a seq
    Create : 'a -> 'a
    Update : 'a -> 'a option
    Delete : int -> unit
    GetById : int -> 'a option
    UpdateById : int -> 'a -> 'a option
    IfExist : int -> bool
  }

  let fromJson<'a> json =
    JsonConvert.DeserializeObject(json, typeof<'a>) :?> 'a

  let getResourceFromReq<'a> (req : HttpRequest) =
    let getString rawForm =
      System.Text.Encoding.UTF8.GetString(rawForm)      
    req.rawForm |> getString |> fromJson<'a>

  let JSON v =
    let s = new JsonSerializerSettings()
    s.ContractResolver <- new CamelCasePropertyNamesContractResolver()
    JsonConvert.SerializeObject(v, s)
    |> OK
    >=> Writers.setMimeType "application/json; charset=utf-8"

  let rest resName r =
    let rPath = "/" + resName
    let getAll = warbler (fun _ -> r.GetAll () |> JSON)  // MAKE IT LAZY!!! This Webpart with the warbler will be lazy (run only when called)
    let badReq = BAD_REQUEST "Resource not found"
    let handleRes reqError = function
      | Some x -> x |> JSON
      | _ -> reqError

    let resourceIdPath = 
      let path = rPath + "/%d" 
      new PrintfFormat<(int -> string),unit,string,string,int>(path)

    let deleteResourceById id = 
      r.Delete id
      NO_CONTENT

    let getResourceById = 
      r.GetById >> handleRes (NOT_FOUND "Resource not found" ) 
    
    let updateResourceById id = 
      request (getResourceFromReq >> (r.UpdateById id) >> handleRes badReq)

    let ifResourceExist id = 
      match id |> r.IfExist with 
      | true -> OK ""
      | false -> NOT_FOUND "Resource not found"

    choose [
      path rPath >=> choose [
        GET >=> getAll
        POST >=> request (getResourceFromReq >> r.Create >> JSON)
        PUT >=> request (getResourceFromReq >> r.Update >> handleRes badReq)
      ]
      DELETE >=> pathScan resourceIdPath deleteResourceById
      GET >=> pathScan resourceIdPath getResourceById
      PUT >=> pathScan resourceIdPath updateResourceById
      HEAD >=> pathScan resourceIdPath ifResourceExist
    ]

