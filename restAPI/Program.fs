﻿open Suave
open Suave.Web
open Suave.Http
open Suave.Successful

open SuaveRestApi.Db
open SuaveRestApi.Rest

[<EntryPoint>]
let main argv =
  let anotherWP = rest "something" {
    GetAll = Other.getSomething
    GetById = Other.getSmtg
    Create = Other.createSmtg
    Update = Other.updateSmtg
    UpdateById = Other.updateSmtgById
    Delete = Other.deleteSmtg
    IfExist = Other.ifSmtgExists
  }
  let personWebPart = rest "people" {
    GetAll = People.getPeople
    GetById = People.getPerson
    Create = People.createPerson
    Update = People.updatePerson
    UpdateById = People.updatePersonById
    Delete = People.deletePerson
    IfExist = People.ifPersonExists
  }

  startWebServer defaultConfig (
    choose [
      personWebPart
      anotherWP
    ]
  )
  0
