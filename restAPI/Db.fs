namespace SuaveRestApi.Db

open System.Collections.Generic

  type Person = {
    Id : int
    Name : string
    Age : int
    Email : string
  }

  type Something = 
    | Smt of string
  
  module People =
    let private pStorage = new Dictionary<int, Person>()

    let getPeople () =
      pStorage.Values :> seq<Person>

    let getPerson pId = 
      match pStorage.ContainsKey(pId) with
      | true -> Some pStorage.[pId]
      | _ -> None

    let ifPersonExists id =
      pStorage.ContainsKey id

    let createPerson p = 
      let id = pStorage.Values.Count + 1
      let np = {p with Id = id }
      pStorage.Add(id, np)
      np

    let updatePersonById pId p = 
      if pStorage.ContainsKey(pId) then
        let updP = { p with Id = pId }
        pStorage.[pId] <- updP
        Some updP
      else
        None
      
    let updatePerson p =
      updatePersonById p.Id p

    let deletePerson pId =
      pStorage.Remove(pId) |> ignore


  module Other =
    let getSomething () = 
      Seq.singleton ("Something all" |> Smt)

    let getSmtg pId = 
      sprintf "Something %d" pId |> Smt |> Some

    let ifSmtgExists id = true

    let createSmtg s = s

    let updateSmtgById id s = 
      s |> Some
      
    let updateSmtg s =
      updateSmtgById 123 s

    let deleteSmtg id = ()
