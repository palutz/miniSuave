namespace MiniSuave

open System

module AP = 
  let (|Eq|_|) exp myv = 
    if exp = myv then Some ()
    else None

  let (|EqStr|_|) (str : string) (cmp : string) = 
    match str.CompareTo cmp with 
      | Eq 0 -> Some ()
      | _ -> None


module MainModule =
  open AP
  open ReqParser
  open Domain.Http

  let matchVerb (v: string) = 
    match v with 
      | ReqType rt -> Some RequestType.GET
      | EqStr "POST"-> Some RequestType.POST
      | _ -> None

  let createRequestType req route =
    match req with 
      | ReqType r -> Some { ReqType = r; Route = route }
      | _ -> None

  ///<summary> Split the request and return the resulting structure wrapped in an Option
  let parseRequest (input : System.String) =
    let l = input.Split([|';'|]) |> Array.toList
    match l with
      | [] -> None
      | [ ReqType x ] -> Some {ReqType = x; Route = String.Empty }
      | (ReqType x)::xs -> Some {ReqType = x; Route = xs.Head }
      | _ -> None

  [<EntryPoint>]
  let main argv =
    let rec innerLoop() = 
      printfn "Enter your commnad: (#quit; to exit. #help; for help) :"
      let cmd = System.Console.ReadLine()
      match cmd with 
      | EqStr "#quit;" _ -> Console.WriteLine "That's all folks!!!"
      | EqStr "#help;" _ -> 
                          Console.WriteLine "We are in beta! Sorry dude, no help."
                          innerLoop()
      | _ -> 
            match cmd |> parseRequest with 
              | Some x -> Console.WriteLine(x.ToString())
                          innerLoop()
              | None -> Console.WriteLine("Command not recognised")
                        innerLoop()
  
    innerLoop()
    0
