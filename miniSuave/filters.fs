namespace MiniSuave

module Filters = 
    
  open Domain.Http

  // (Context -> bool) -> Context -> Async<Context option>
  let iff condition context = 
    if condition context then
      context |> Some |> async.Return
    else
      None |> async.Return

  let GET = iff (fun ctx -> ctx.Request.ReqType = GET) 
  let POST = iff (fun ctx -> ctx.Request.ReqType = POST) 
  let Path path = iff (fun ctx -> ctx.Request.Route = path)


module Combinators = 

  let (>=>) first second ctx =
    async {
      let! fstCtx = first ctx
      match fstCtx with
      | None -> return None
      | Some ctx1 -> 
        let! secCtx = second ctx1
        return secCtx
    }

  // General compose / combinator 
  (* let (>==>) f1 f2 x = 
    async {
      let! r1 = f1 x
      match r1 with
        | None -> return None
        | Some rx -> 
                let! r2 = f2 rx
                return r2
    }
*)
