namespace MiniSuave

module Domain = 

  module Http =
    //
    // type for our server
    //
    type RequestType = GET | POST | PUT
    type Request = {
      Route : string
      ReqType : RequestType
    }
    type Response = {
      Content: string
      StatusCode: int
    }
    type Context = {
      Request: Request
      Response: Response
    }
    // like this our web server will be Context -> Async<Context>
    // but we want to manage also the case where we don't have a context back (not the request)
    // first version
    // type Webpart = Context -> Async<Context>
    // the answer could be present or not so adding option
    type Webpart = Context -> Async<Context option>


  // first webpart - success one
  module Successful =
    open Http

    let OK content context = 
      { context with Response = { Content = content; StatusCode = 200 } }
      |> Some 
      |> async.Return

