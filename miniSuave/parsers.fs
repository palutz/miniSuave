namespace MiniSuave

module ReqParser =
  open Domain.Http
  
  let (|ReqType|_|) (r: string) =
    match r.ToUpper() with 
      | "GET" -> Some RequestType.GET
      | "POST" -> Some RequestType.POST
      | "PUT" -> Some RequestType.PUT
      | _ -> None