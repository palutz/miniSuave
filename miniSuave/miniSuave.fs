namespace MiniSuave

// We can model a simple webserver as a function getting a request
// and returning a response (with ow without async)

// first webpart - success one
module Successful =
  open Domain.Http

  let OK content context = 
    { context with Response = { Content = content; StatusCode = 200 } }
    |> Some 
    |> async.Return


module Console =
  open Domain.Http

  let runWebPart inputCtx webpart = 
    async {
      let! outputCtx = webpart inputCtx
      match outputCtx with 
      | Some ctx -> 
            printfn "-------------"
            printfn "Code: %d " ctx.Response.StatusCode
            printfn "Content: %s" ctx.Response.Content
            printfn "-------------"
      | _ -> printfn "Error!!!"
    } |> Async.RunSynchronously

// this is a lib we don;'t need the main method
  // let main argv = 
  //   let req = { Route = ""; Type = MySuave.Http.GET } 
  //   let resp = { Content = ""; StatusCode = 0 }
  //   let ctx = { Request = req; Response = resp }

  //   runWebPart ctx (Successful.OK  "Hello, webpart")
